// load the things we need
const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const fs = require('fs');

// Retrieve images
app.use(express.static('public'));

// set the view engine to ejs
app.set('view engine', 'ejs');

// app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

let rawdataDB = fs.readFileSync('data/books_db.json');
let bookDB = JSON.parse(rawdataDB);

let rawdataDB2 = fs.readFileSync('data/cd_db.json');
let cdDB = JSON.parse(rawdataDB2);

// index page
app.get('/', (req, res) => {
    res.render('pages/index');
});

// books page
app.get('/books', (req, res) => {
    res.render('pages/books', {
        bookDB: bookDB.books
    });
});

// book detail page
app.get('/book/:id', (req, res) => {
    console.log(req.params.id)
});

// cds page
app.get('/cds', (req,res) => {
    res.render('pages/cds', {
        cdDB: cdDB.cds
    });
});

// cds detail page
app.get('/cds/:id', (req, res) => {
    console.log(req.params.id)
});

// about page
app.get('/about', (req, res) => {
    res.render('pages/about');
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});


